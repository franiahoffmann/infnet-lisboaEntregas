import {Container} from '../../components/Container';
import {CustomText} from '../../components/CustomText';
import React from 'react';

export function OrdersView() {
  return (
    <Container>
      <CustomText>Aqui vai ter os pedidos</CustomText>
    </Container>
  );
}
